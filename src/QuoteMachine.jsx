import React from "react";
import quotes from "./quotes.json";


class QuoteMachine extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            author: null,
            quotes: quotes,
            text: null,
            tweetLink: null,
        };
    }
    render() {
        return (
            <div id="quote-box">
                <div className="quote-text">
                    <span id="text">{this.state.text}</span>
                </div>
                <div className="quote-author">- <span id="author">{this.state.author}</span></div>
                <div className="buttons">
                    <a
                        className="button"
                        id="tweet-quote"
                        title="Tweet this quote!"
                        target="_top"
                        href={this.state.tweetLink}
                    >
                        <i className="fa fa-twitter"></i>
                    </a>
                    <button
                        className="button"
                        id="new-quote"
                        onClick={() => this.getRandomQuote()}
                    >
                        New quote
                    </button>
                </div>
            </div>
        )
    }
    componentDidMount() {
        this.getRandomQuote();
    }
    getRandomQuote() {
        const randomIndex = Math.floor(Math.random() * this.state.quotes.length);
        const quote = this.state.quotes[randomIndex];
        const tweetLink = (
            'https://twitter.com/intent/tweet?hashtags=quotes&related=freecodecamp&text=' +
            encodeURIComponent('"' + quote.text + '" ' + quote.author)
        )
        const newState = { author: quote.author, text: quote.text , tweetLink: tweetLink }
        this.setState(newState);
    }
}


export default QuoteMachine