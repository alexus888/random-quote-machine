import './style.scss';
import React from "react";
import ReactDOM from "react-dom";
import QuoteMachine from "./QuoteMachine";



ReactDOM.render(
    <QuoteMachine />,
    document.getElementById('app')
);

